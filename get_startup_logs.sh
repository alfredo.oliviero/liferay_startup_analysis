#!/bin/bash

DEST_FOLDER=logs
# Remove existing logs directory and create a new one
mkdir -p $DEST_FOLDER

# Define an array of servers
SERVERS=( "lr62-pre-01" "lr62-pre-02" "lr62-dev"  "lr62-prod-01" "lr62-prod-02")

# Loop through each server and execute the grep command, saving the results
for SERVER in "${SERVERS[@]}"; do
    ssh "$SERVER" 'grep -R "Server startup in" /home/life/Portal-Bundle/tomcat-7.0.62/logs/*' > "$DEST_FOLDER/startup_$SERVER.log"
done
