import pandas as pd
import matplotlib.pyplot as plt
import re
import os
from datetime import datetime
import json
import argparse

# Function to load data from files
def load_data(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    
    data = []
    for line in lines:
        date = re.search(r'(\d{4}-\d{2}-\d{2})', line).group(0)
        startup_time = re.search(r'Server startup in (\d+) ms', line).group(1)
        data.append([date, int(startup_time)])
        
    df = pd.DataFrame(data, columns=["Date", "StartupTime"])
    df["Date"] = pd.to_datetime(df["Date"])
    df["StartupTimeSeconds"] = df["StartupTime"] / 1000  # convert to seconds
    return df

# Function to find files and labels in the log directory
def find_files_and_labels(logs_dir):
    files_and_labels = []
    for file_name in os.listdir(logs_dir):
        if file_name.endswith(".log"):
            file_path = os.path.join(logs_dir, file_name)
            label = re.search(r'startup_(.+)\.log', file_name).group(1)  # Infer label from file name
            files_and_labels.append((file_path, label))
    return files_and_labels

# Parsing command-line arguments
parser = argparse.ArgumentParser(
    description='Process server startup logs.',
    epilog='''
Examples of usage:

1. Use log directory and results directory with a specific date range:
   python analyze_startup.py --log-dir ./logs --results-dir ./results --min-date 2023-07-01 --max-date 2023-12-31

2. Use specific log files with automatic labels:
   python analyze_startup.py -f ./logs/startup_lr62-prod-01.log -f ./logs/startup_lr62-prod-02.log

3. Use specific log files with custom labels:
   python analyze_startup.py -f ./logs/startup_lr62-prod-01.log -l prod01 -f ./logs/startup_lr62-prod-02.log -l prod02

4. Combine log directory and specific files:
   python analyze_startup.py --log-dir ./logs -f ./logs/startup_lr62-dev.log -l dev --results-dir ./results --min-date 2023-07-01
    ''',
    formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument('--log-dir', type=str, default=None, help='Directory containing log files')
parser.add_argument('--results-dir', type=str, default='./results', help='Directory to save results')
parser.add_argument('--max-date', type=str, default=None, help='Maximum date for filtering logs (YYYY-MM-DD)')
parser.add_argument('--min-date', type=str, default='2023-07-01', help='Minimum date for filtering logs (YYYY-MM-DD)')
parser.add_argument('-f', '--file', type=str, action='append', help='Explicit log file')
parser.add_argument('-l', '--label', type=str, action='append', help='Label for explicit log file')

args = parser.parse_args()

# Handling explicit files and labels
files_and_labels = []
if args.file:
    for i, file in enumerate(args.file):
        label = args.label[i] if args.label and len(args.label) > i else re.search(r'startup_(.+)\.log', file).group(1)
        files_and_labels.append((file, label))

# If log_dir is defined, populate files_and_labels from the directory
if args.log_dir:
    files_and_labels += find_files_and_labels(args.log_dir)

# Print the list of calculated files and labels in a readable format
print("files_and_labels = [")
for file_path, label in files_and_labels:
    print(f'    ("{file_path}", "{label}"),')
print("]")

# Convert date strings to datetime objects
min_date = pd.to_datetime(args.min_date) if args.min_date else None
max_date = pd.to_datetime(args.max_date) if args.max_date else None

# Use the current date if max_date_str is None
current_date_str = datetime.now().strftime("%Y-%m-%d")
date_range_str = f"{args.min_date}_to_{args.max_date if args.max_date else current_date_str}"

# Ensure the results directory exists
os.makedirs(args.results_dir, exist_ok=True)

# Plot setup
plt.figure(figsize=(14, 7))

# Dictionary to hold data for JSON export
export_data = {}

# Iterate through each file and label
for file_path, label in files_and_labels:
    # Load data from file
    df = load_data(file_path)

    # Filter data based on date range
    if min_date is not None:
        df = df[df["Date"] >= min_date]
    if max_date is not None:
        df = df[df["Date"] <= max_date]

    # Sort data by date
    df_sorted = df.sort_values(by="Date")
    plt.plot(df_sorted["Date"], df_sorted["StartupTimeSeconds"], marker='o', label=label)

    # Add data to the dictionary for JSON export
    export_data[label] = df_sorted[["Date", "StartupTimeSeconds"]].to_dict(orient="records")

# Create the filename base using the date range and labels
labels_str = "_".join([label for _, label in files_and_labels])
filename_base = f"{args.results_dir}/server_startup_{labels_str}_{date_range_str}"
png_filename = f"{filename_base}.png"
json_filename = f"{filename_base}.json"

plt.title("Server Startup Time Over Time (Filtered, in seconds)")
plt.xlabel("Date")
plt.ylabel("Startup Time (seconds)")
plt.grid(True)
plt.legend()
plt.xticks(rotation=45)
plt.tight_layout()

# Save the plot as a PNG file
plt.savefig(png_filename)

# Save the filtered data as a JSON file
with open(json_filename, 'w') as json_file:
    json.dump(export_data, json_file, indent=4, default=str)

plt.show()
