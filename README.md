## creazione venv e installazione dipendenze

```sh
python -m venv venv 
. ./venv/bin/activate
pip install -r requirements.txt
```

## fetch log startup

```sh
./get_startup_logs.sh
```

## avvio script analisi
```sh
python analyze_startup.py --log-dir ./logs --results-dir ./results --min-date 2023-07-01 --max-date 2023-12-31
```